#!/bin/bash
PATH=/usr/local/bin:/usr/bin/
terraform apply -auto-approve && sleep 30 && ansible-playbook -i /etc/ansible/terraform.py ansible-install-nginx.yml -b -u root --private-key .ssh/id_rsa && \
ansible-playbook -i /etc/ansible/terraform.py ansible-install-keepalived.yml -b -u root --private-key .ssh/id_rsa && \
ansible-playbook -i /etc/ansible/terraform.py ansible-install-haproxy.yml -b -u root --private-key .ssh/id_rsa
