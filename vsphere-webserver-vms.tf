resource "vsphere_virtual_machine" "web-01" {
  name             = "web-01"
  resource_pool_id = data.vsphere_resource_pool.pool.id
  datastore_id     = data.vsphere_datastore.datastore.id
  num_cpus = lookup(var.cpu_amount, var.web_plan)
  memory   = lookup(var.mem_amount, var.web_plan)
  guest_id = data.vsphere_virtual_machine.template.guest_id
  network_interface {
    network_id = data.vsphere_network.network.id
  }
  disk {
    label = "disk0"
    size  = lookup(var.storage_sizes, var.web_plan)
    thin_provisioned = false
  }
  clone {
    template_uuid = data.vsphere_virtual_machine.template.id
    customize {
      linux_options {
        host_name = "web-01"
        domain    = var.machine_domain
      }
      network_interface {
        ipv4_address = var.web-01-ip
        ipv4_netmask = var.vsphere_network_netmask
      }
      ipv4_gateway = var.vsphere_network_gateway
      dns_suffix_list = [var.machine_domain]
      dns_server_list = [var.vsphere_network_dns_server]
    }
  }
}
resource "vsphere_virtual_machine" "web-02" {
  name             = "web-02"
  resource_pool_id = data.vsphere_resource_pool.pool.id
  datastore_id     = data.vsphere_datastore.datastore.id
  num_cpus = lookup(var.cpu_amount, var.web_plan)
  memory   = lookup(var.mem_amount, var.web_plan)
  guest_id = data.vsphere_virtual_machine.template.guest_id
  network_interface {
    network_id = data.vsphere_network.network.id
  }
  disk {
    label = "disk0"
    size  = lookup(var.storage_sizes, var.web_plan)
    thin_provisioned = false
  }
  clone {
    template_uuid = data.vsphere_virtual_machine.template.id
    customize {
      linux_options {
        host_name = "web-02"
        domain    = var.machine_domain
      }
      network_interface {
        ipv4_address = var.web-02-ip
        ipv4_netmask = var.vsphere_network_netmask
      }
      ipv4_gateway = var.vsphere_network_gateway
      dns_suffix_list = [var.machine_domain]
      dns_server_list = [var.vsphere_network_dns_server]
    }
  }
}
resource "ansible_host" "web-01" {
    inventory_hostname = vsphere_virtual_machine.web-01.default_ip_address 
                groups = ["web"]
                vars = {
                    network = var.vsphere_network_address
                    netmask = var.vsphere_network_netmask
                }
}
resource "ansible_host" "web-02" {
    inventory_hostname = vsphere_virtual_machine.web-02.default_ip_address
                groups = ["web"]
                vars = {
                    network = var.vsphere_network_address
                    netmask = var.vsphere_network_netmask
                }
}
