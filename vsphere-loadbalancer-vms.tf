resource "vsphere_virtual_machine" "lb-01" {
  name             = "lb-01"
  resource_pool_id = data.vsphere_resource_pool.pool.id
  datastore_id     = data.vsphere_datastore.datastore.id
  num_cpus = lookup(var.cpu_amount, var.lb_plan)
  memory   = lookup(var.mem_amount, var.lb_plan)
  guest_id = data.vsphere_virtual_machine.template.guest_id
  network_interface {
    network_id = data.vsphere_network.network.id
  }
  disk {
    label = "disk0"
    size  = lookup(var.storage_sizes, var.lb_plan)
    thin_provisioned = false
  }
  clone {
    template_uuid = data.vsphere_virtual_machine.template.id
    customize {
      linux_options {
        host_name = "lb-01"
        domain    = var.machine_domain
      }
      network_interface {
        ipv4_address = var.lb-01-ip
        ipv4_netmask = var.vsphere_network_netmask
      }
      ipv4_gateway = var.vsphere_network_gateway
      dns_suffix_list = [var.machine_domain]
      dns_server_list = [var.vsphere_network_dns_server]
    }
  }
}
resource "vsphere_virtual_machine" "lb-02" {
  name             = "lb-02"
  resource_pool_id = data.vsphere_resource_pool.pool.id
  datastore_id     = data.vsphere_datastore.datastore.id
  num_cpus = lookup(var.cpu_amount, var.lb_plan)
  memory   = lookup(var.mem_amount, var.lb_plan)
  guest_id = data.vsphere_virtual_machine.template.guest_id
  network_interface {
    network_id = data.vsphere_network.network.id
  }
  disk {
    label = "disk0"
    size  = lookup(var.storage_sizes, var.lb_plan)
    thin_provisioned = false
  }
  clone {
    template_uuid = data.vsphere_virtual_machine.template.id
    customize {
      linux_options {
        host_name = "lb-02"
        domain    = var.machine_domain
      }
      network_interface {
        ipv4_address = var.lb-02-ip
        ipv4_netmask = var.vsphere_network_netmask
      }
      ipv4_gateway = var.vsphere_network_gateway
      dns_suffix_list = [var.machine_domain]
      dns_server_list = [var.vsphere_network_dns_server]
    }
  }
}
resource "ansible_host" "lb-01" {
    inventory_hostname = vsphere_virtual_machine.lb-01.default_ip_address 
                groups = ["lb"]
                vars = {
                    lb-vip = var.lb-vip
                    vrrp-id = "1"
                    network = var.vsphere_network_address
                    netmask = var.vsphere_network_netmask
                }
}
resource "ansible_host" "lb-02" {
    inventory_hostname = vsphere_virtual_machine.lb-02.default_ip_address
                groups = ["lb"]
                vars = {
                    lb-vip = var.lb-vip
                    vrrp-id = "1"
                    network = var.vsphere_network_address
                    netmask = var.vsphere_network_netmask
                }
}
