#!/bin/bash
PATH=/usr/local/bin:/usr/bin/
echo "---------------------------------------------------------------------------------------------------------"
if curl $(terraform state show ansible_host.lb-01 | grep lb-vip | awk '{print $3}' | sed 's/"//g')\
 | grep 'Welcome to CentOS|Test Page for the Nginx HTTP Server on Red Hat Enterprise Linux' &> /dev/null; then
  echo
  echo "Exit Code:" $?
  echo "Webpage up & running"
fi
echo "---------------------------------------------------------------------------------------------------------"
