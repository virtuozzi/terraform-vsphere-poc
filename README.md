# Webserver and Loadbalancer deployment on vSphere (PoC/Combination of Terraform with Ansible)

- Highly available web-setup used as base for web-applications
- Can be easily extended by additional webserver- or loadbalancer-nodes
- Current setup uses four vsphere-machines (2x nginx webserver & 2x haproxy/keepalived loadbalancer)
- Ssh/web access from within local LAN allowed
- Keepalived used for virtual ip sharing on loadbalancers (VRRP)
- Haproxy on loadbalancers uses both web instances as backends
- Jinja2 template sourcing in use (*.j2)

## Prerequisites

- Terraform 0.12
- Ansible 2.9 or higher
- Ansible Provider for Terraform (for example located at ~/.terraform.d/plugins/registry.github.com.local/nbering/ansible/\<VERSION\>/\<OS\>_\<ARCH\>)
https://github.com/nbering/terraform-provider-ansible/
- Inventory Script:
https://github.com/nbering/terraform-inventory/ (for example located at /etc/ansible/terraform.py)
- Prepared and OS-ready CentOS or RHEL template which will be used within variables.tf

## Getting Started

- Clone this repo
- Set up your ssh-key under ~/.ssh/id_rsa
- Set up variables @variables.tf
- Use the terraform binary locally (terraform plan & apply) and ansible afterwards in conjunction with the ansible provider and the playbooks 
```
ansible-playbook -i /etc/ansible/terraform.py ansible-install-nginx.yml -b -u root --private-key .ssh/id_rsa && \
ansible-playbook -i /etc/ansible/terraform.py ansible-install-keepalived.yml -b -u root --private-key .ssh/id_rsa && \
ansible-playbook -i /etc/ansible/terraform.py ansible-install-haproxy.yml -b -u root --private-key .ssh/id_rsa
```
Another way of buildung up, is by using the build-up script (scenario2-build.bash).

**Be cautious with the bash-scripts, especially scenario2-destroy.bash, since these scripts doesn't ask for your "ok" to build or destroy the setup.
Use these scripts for testing purposes.**

## Important notes

- Terraform backend should be bound to aws s3 bucket to obtain consistent state and collaboration functionality. 
Update "your_bucket" and "your_region" according to your settings.

main.tf:

```
terraform {  
    backend "s3" {
        bucket = "**your_bucket**"
        key    = "terraform.tfstate"    
        region = "**your_region**"  
    }
}
```


# Authors

* **Michel Drozdz** - *Initial work* 

# License

This project is licensed under the GNU GPLv3 License.

# Acknowledgments

- Terraform Devs and Documentation
- https://github.com/nbering
