variable "vsphere_user" {
    type = string
    default = "vsphere_service_account@domain.local"
}
variable "vsphere_password" {
    type = string
    default = "supersecretpassword"
}
variable "vsphere_server" {
    type = string
    default = "vsphere_server_fqdn"
}
variable "vsphere_dc" {
    type = string
    default = "vSphere Datacenter"
}
variable "vsphere_datastore" {
    type = string
    default = "my_datastore_to_use"
}
variable "vsphere_resource_pool" {
    type = string
    default = "Resources"
}
variable "vsphere_template" {
    type = string
    default = "my_template"
}
variable "plans" {
    type    = map
    default = {
        "Bronze" = "1xCPU-2GB"
        "Silver" = "2xCPU-4GB"
        "Gold"   = "4xCPU-8GB"
    }
}
variable "lb_plan" {
    type    = string
    default = "Bronze"
}
variable "web_plan" {
    type    = string
    default = "Silver"
}
variable "storage_sizes" {
    type    = map
    default = {
        "1xCPU-2GB" = "25"
        "2xCPU-4GB" = "40"
        "4xCPU-8GB" = "80"
    }
}
variable "cpu_amount" {
    type    = map
    default = {
        "1xCPU-2GB" = "1"
        "2xCPU-4GB" = "2"
        "4xCPU-8GB" = "4"
    }
}
variable "mem_amount" {
    type    = map
    default = {
        "1xCPU-2GB" = "2048"
        "2xCPU-4GB" = "4096"
        "4xCPU-8GB" = "8192"
    }
}
variable "vsphere_network" {
    type = string
    default = "my_network_to_use"
}
variable "vsphere_network_address" {
    type = string
    default = "10.132.10.0"
}
variable "vsphere_network_netmask" {
    type = string
    default = "24"
}
variable "vsphere_network_gateway" {
    type = string
    default = "10.132.10.1"
}
variable "vsphere_network_dns_server" {
    type = string
    default = "10.132.10.10"
}
variable "machine_domain" {
    type = string
    default = "domain.local"
}
variable "web-01-ip" {
    type = string
    default = "10.132.10.11"
}
variable "web-02-ip" {
    type = string
    default = "10.132.10.12"
}
variable "lb-01-ip" {
    type = string
    default = "10.132.10.13"
}
variable "lb-02-ip" {
    type = string
    default = "10.132.10.14"
}
variable "lb-vip" {
    type = string
    default = "10.132.10.15"
}
